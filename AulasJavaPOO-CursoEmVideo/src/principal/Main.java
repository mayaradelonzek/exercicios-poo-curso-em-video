package principal;

import java.util.Scanner;

import executores.Executor;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Integer nroAula;

		do {
			System.out.println("Escolha uma aula [1-14] que deseja executar " + "ou '0' para sair:\n");
			nroAula = scanner.nextInt();
			executarAula(nroAula);
		} while (nroAula != 0);

		scanner.close();
		System.out.println("Programa finalizado!");
	}

	private static void executarAula(Integer nroAula) {
		switch (nroAula) {
		case 0:
			break;
		case 1:
			Executor.executarAula1();
			break;
		case 2:
			Executor.executarAula2();
			break;
		case 3:
			Executor.executarAula2b();
			break;

		case 4:
			Executor.executarAula3();
			break;

		case 5:
			Executor.executarAula4();
			break;

		case 6:
			Executor.executarAula5();
			break;

		case 7:
			Executor.executarAula5b();
			break;

		case 8:
			Executor.executarAula6();
			break;

		case 9:
			Executor.executarAula7();
			break;

		case 10:
			Executor.executarAula9();
			break;

		case 11:
			Executor.executarAula10();
			break;

		case 12:
			Executor.executarAula11();
			break;

		case 13:
			Executor.executarAula12();
			break;

		case 14:
			Executor.executarAula13();
			break;

		case 15:
			Executor.executarAula14();
			break;

		default:
			System.out.println("Aula inv�lida!");
			break;
		}
	}

}
