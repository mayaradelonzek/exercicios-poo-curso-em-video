package executores;

import aula1.Aula1;
import aula2.Aula2;
import aula2b.Aula2b;
import aula3.Aula3;
import aula4.Aula4;
import aula5.Aula5;
import aula5b.Aula5b;
import aula6.Aula6;
import aula7.Aula7;
import aula9.Aula9;
import aula10.Aula10;
import aula11.Aula11;
import aula12.Aula12;
import aula13.Aula13;
import aula14.Aula14;
import aula_intf.Aula;

public class Executor {

	public static void executarAula1() {
		Aula aula = new Aula1();
		aula.executar();
	}
	
	public static void executarAula2() {
		Aula aula = new Aula2();
		aula.executar();
	}
	
	public static void executarAula2b() {
		Aula aula = new Aula2b();
		aula.executar();
	}
	
	public static void executarAula3() {
		Aula aula = new Aula3();
		aula.executar();
	}
	
	public static void executarAula4() {
		Aula aula = new Aula4();
		aula.executar();
	}
	
	public static void executarAula5() {
		Aula aula = new Aula5();
		aula.executar();
	}
	
	public static void executarAula5b() {
		Aula aula = new Aula5b();
		aula.executar();
	}
	
	public static void executarAula6() {
		Aula aula = new Aula6();
		aula.executar();
	}
	
	public static void executarAula7() {
		Aula aula = new Aula7();
		aula.executar();
	}	
	
	public static void executarAula9() {
		Aula aula = new Aula9();
		aula.executar();
	}
	
	public static void executarAula10() {
		Aula aula = new Aula10();
		aula.executar();
	}
	
	public static void executarAula11() {
		Aula aula = new Aula11();
		aula.executar();
	}
	
	public static void executarAula12() {
		Aula aula = new Aula12();
		aula.executar();
	}
	
	public static void executarAula13() {
		Aula aula = new Aula13();
		aula.executar();
	}
	
	public static void executarAula14() {
		Aula aula = new Aula14();
		aula.executar();
	}

	
	
}
