package aula2;
public class Calculadora {
	String modelo;
	String cor;
	boolean cientifica;
	boolean ligada;
	
	void status() {
		System.out.println("Modelo: " + this.modelo);
		System.out.println("Cor: " + this.cor);
		System.out.println("Normal ou cientifica? " + this.cientifica);
	}
	
	void calcular() {
		if (ligada == true) {
			System.out.println("Calculadora ligada, pode calcular! ");
		} else {
			System.out.println("Calculadora desligada! Ligue ela para poder calcular! ");
		}
	}
	
	void tipoCientifica() {
		if (this.cientifica == true) {
			System.out.println("Calculadora cientifica! ");
		} else {
			System.out.println("Calculadora normal! ");
		}
	}
	
	
	

}
