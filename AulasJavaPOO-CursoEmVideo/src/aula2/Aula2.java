package aula2;

import aula_intf.Aula;

public class Aula2 implements Aula {
	
	@Override
	public void executar() {
		Exercicios.executarExercicio1();
		Exercicios.executarExercicio2();
	}

}
