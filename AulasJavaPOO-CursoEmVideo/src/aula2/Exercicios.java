package aula2;

public class Exercicios {

	public static void executarExercicio1() {
		Caneta c1 = new Caneta();
		c1.modelo = "Bic";
		c1.cor = "Azul";
		c1.ponta = 0.5f;
		c1.destampar();		
		c1.status();
		c1.rabiscar();
		
		Caneta c2 = new Caneta();
		c2.modelo = "Faber";
		c2.cor = "Preta";
		c2.ponta = 0.7f;
		c2.tampar();
		c2.status();
		c2.rabiscar();
	}
	
	public static void executarExercicio2() {
		Calculadora calc1 = new Calculadora();
		calc1.modelo = "Casius";
		calc1.cor = "Azul e Prata";
		calc1.cientifica = true;
		calc1.ligada = true;
		calc1.status();
		calc1.tipoCientifica();
		calc1.calcular();
		
		System.out.println();
		
		Calculadora calc2 = new Calculadora();
		calc2.modelo = "UmNoveNove";
		calc2.cor = "Preta";
		calc2.cientifica = false;
		calc2.ligada = false;
		calc2.status();
		calc2.tipoCientifica();
		calc2.calcular();
	}
	
}
