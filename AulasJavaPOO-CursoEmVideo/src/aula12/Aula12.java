package aula12;

import aula_intf.Aula;

public class Aula12 implements Aula {		

		@Override
		public void executar() {
		Mamifero mamifero = new Mamifero();
		Reptil reptil = new Reptil();
		Peixe peixe = new Peixe();
		Ave ave = new Ave();
		Canguru canguru = new Canguru();
		Cachorro cachorro = new Cachorro();
		Cobra cobra = new Cobra();
		Tartaruga tartaruga = new Tartaruga();
		Goldfish goldfish = new Goldfish();
		Arara arara = new Arara();
		
		mamifero.alimentar();
		reptil.emitirSom();
		peixe.locomover();
		ave.locomover();
		canguru.locomover();
		cachorro.emitirSom();
		cobra.alimentar();
		tartaruga.emitirSom();
		goldfish.locomover();
		arara.alimentar();
		
	}
}
