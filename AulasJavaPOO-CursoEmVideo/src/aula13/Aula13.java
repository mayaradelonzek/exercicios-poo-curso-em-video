package aula13;

import aula_intf.Aula;

public class Aula13 implements Aula {		

		@Override
		public void executar() {
		
		Cachorro cachorro = new Cachorro();
		cachorro.reagir("Ol�");
		cachorro.reagir("Vai apanhar");
		cachorro.reagir(11, 45);
		cachorro.reagir(21, 00);
		cachorro.reagir(true);
		cachorro.reagir(false);
		cachorro.reagir(2, 12.5f);
		cachorro.reagir(17, 4.5f);

	}

}
