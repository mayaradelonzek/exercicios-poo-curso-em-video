package aula9;

public class Livro implements Publicacao {
	//Atributos
	private String titulo;
	private String autor;
	private int totalPaginas;
	private int paginaAtual;
	private boolean aberto;
	private Pessoa leitor;      //Agrega��o
	
	//M�todos p�blicos	
	
	@Override
	public String detalhes() {
		return "Detalhes do Livro:\n T�tulo: " + titulo + "\n Autor: " + autor + "\n Total de P�ginas: " + totalPaginas + "\n P�gina Atual: "
				+ paginaAtual + "\n Aberto: " + aberto + "\n [Leitor: " + leitor.getNome() + " - Idade: " + leitor.getIdade() 
				+ " - Sexo: " + leitor.getSexo() + "]";
	}

		//M�todos especiais
	public Livro(String titulo, String autor, int totalPaginas, Pessoa leitor) {
		super();
		this.titulo = titulo;
		this.autor = autor;
		this.aberto = false;
		this.paginaAtual = 0;
		this.totalPaginas = totalPaginas;
		this.leitor = leitor;
	}
		
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getTotalPaginas() {
		return totalPaginas;
	}

	public void setTotalPaginas(int totalPaginas) {
		this.totalPaginas = totalPaginas;
	}

	public int getPaginaAtual() {
		return paginaAtual;
	}

	public void setPaginaAtual(int paginaAtual) {
		this.paginaAtual = paginaAtual;
	}

	public boolean getAberto() {
		return aberto;
	}

	public void setAberto(boolean aberto) {
		this.aberto = aberto;
	}

	public Pessoa getLeitor() {
		return leitor;
	}

	public void setLeitor(Pessoa leitor) {
		this.leitor = leitor;
	}

		
	//M�todos abstratos
	@Override
	public void abrir() {
		this.aberto = true;
		System.out.println("O livro est� aberto! \n");
	}

	@Override
	public void fechar() {
		this.aberto = false;
		System.out.println("O livro est� fechado! \n");
	}

	@Override
	public void folhear(int pagina) {		
		if (pagina > this.totalPaginas) {
			this.paginaAtual = 0;
			
		} else {
		
		this.paginaAtual = pagina;
		}
	}

	@Override
	public void avancarPagina() {
		this.paginaAtual++;
		
	}

	@Override
	public void voltarPagina() {
		this.paginaAtual--;
		
	}
	
	
}
