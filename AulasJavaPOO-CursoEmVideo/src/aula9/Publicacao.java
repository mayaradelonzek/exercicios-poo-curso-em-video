package aula9;

interface Publicacao {
	public void abrir();
	public void fechar();
	public void folhear(int pagina);
	public void avancarPagina();
	public void voltarPagina();
	public String detalhes();
	
}
