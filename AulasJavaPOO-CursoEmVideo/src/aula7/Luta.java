package aula7;

import java.util.Random;

class Luta {
	//Atributos
	public Lutador desafiado;
	private Lutador desafiante;
	private int rounds;
	private boolean aprovada; 
	
	//M�todos P�blicos
	public void marcarLuta(Lutador l1, Lutador l2) {                      //     MESMA COISA DE ESCREVER ASSIM: 
		if (l1.getCategoria().equals(l2.getCategoria()) && (l1 != l2)) { // (l1.getCategoria() == l2.getCategoria())
			this.aprovada = true;
			this.desafiado = l1;
			this.desafiante = l2;
		} else {
			this.aprovada = false;
			this.desafiado = null;
			this.desafiante = null;
		}
		
	}
	
	public void lutar() {
		if (this.aprovada) {
			System.out.println("### DESAFIADO ###");
			this.desafiado.apresentar();
			System.out.println("### DESAFIANTE ###");
			this.desafiante.apresentar();
			
			Random aleatorio = new Random();
			int vencedor = aleatorio.nextInt(3); // 0, 1, 2
			switch(vencedor) {
				case 0: // Empate
					System.out.println("Empatou a luta!");
					this.desafiado.empatarLuta();
					this.desafiante.empatarLuta();
					break;
					
				case 1: // Desafiado vence
					System.out.println(this.desafiado.getNome() + " venceu a luta");
					this.desafiado.ganharLuta();
					this.desafiante.perderLuta();
					break;
					
				case 2: // Desafiante vence
					System.out.println(this.desafiante.getNome() + " venceu a luta");
					this.desafiado.perderLuta();
					this.desafiante.ganharLuta();
					break;
			}
		} else {
			System.out.println("A luta n�o pode acontecer!");
		}
	}
	
	//M�todos Especiais
	public Lutador getDesafiado() {
		return desafiado;
	}

	public void setDesafiado(Lutador desafiado) {
		this.desafiado = desafiado;
	}

	public Lutador getDesafiante() {
		return desafiante;
	}

	public void setDesafiante(Lutador desafiante) {
		this.desafiante = desafiante;
	}

	public int getRounds() {
		return rounds;
	}

	public void setRounds(int rounds) {
		this.rounds = rounds;
	}

	public boolean getAprovada() {
		return aprovada;
	}

	public void setAprovada(boolean aprovada) {
		this.aprovada = aprovada;
	}
	
	
	

}
