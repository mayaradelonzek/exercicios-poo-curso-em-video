package aula7;

class Lutador {
	//Atributos
	private String nome;
	private String nacionalidade;
	private String categoria;
	private float altura;
	private float peso;
	private int idade;
	private int vitorias;
	private int derrotas;
	private int empates;
	
	//Metodos publicos
	public void apresentar() {
		System.out.println("------------------------------------------------------");
		System.out.println("CHEGOU A HORA! ");
		System.out.println("Apresentamos o lutador " + this.nome);
		System.out.println("Diretamente do seu pa�s natal " + this.nacionalidade);
		System.out.print("Com " + this.idade + " anos, " + this.altura + " de altura ");
		System.out.println("e pesando " + this.peso + " kg ");
		System.out.print("Possui " + this.vitorias + " vit�rias, ");
		System.out.print(this.derrotas + " derrotas e ");
		System.out.println(this.empates + " empates");
		System.out.println("");
	}
	
	public void status() {
		System.out.println(this.nome + " � um peso " + this.categoria);
		System.out.println("Ganhou " + this.vitorias + " vezes");
		System.out.println("Perdeu " + this.derrotas + " vezes");
		System.out.println("Empatou " + this.empates + " vezes");
		
	}
	
	public void ganharLuta() {
		//this.vitorias = this.vitorias + 1;
		this.setVitorias(this.getVitorias() + 1);
	}
	
	public void perderLuta() {
		this.setDerrotas(this.getDerrotas() + 1);
	}
	
	public void empatarLuta() {
		this.setEmpates(this.getEmpates() + 1);
	}
	
	//Metodos Especiais
	public Lutador (String nome, String nacionalidade, int idade, float altura, float peso, int vitorias, int derrotas, int empates) {
		this.nome = nome;
		this.nacionalidade = nacionalidade;
		this.idade = idade;
		this.altura = altura;
		this.peso = peso;
		this.vitorias = vitorias;
		this.empates = empates;
		this.derrotas = derrotas;
		setCategoria();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public float getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
		this.setCategoria();
	}
	
	public String getCategoria() {
		return categoria;
	}

	private void setCategoria() {
		
		if (this.peso < 52.2) {
			this.categoria = "Inv�lido";
		} else if (this.peso <= 70.3) {
			this.categoria = "Leve";
		} else if (this.peso <= 83.9) {
			this.categoria = "M�dio";
		} else if (this.peso <= 120.2) {
			this.categoria = "Pesado";
		} else {
			this.categoria = "Inv�lido";
		}
	}

	public int getVitorias() {
		return vitorias;
	}

	public void setVitorias(int vitorias) {
		this.vitorias = vitorias;
	}

	public int getDerrotas() {
		return derrotas;
	}

	public void setDerrotas(int derrotas) {
		this.derrotas = derrotas;
	}

	public int getEmpates() {
		return empates;
	}

	public void setEmpates(int empates) {
		this.empates = empates;
	}
	
	
	
}
