package aula7;

import aula_intf.Aula;

public class Aula7 implements Aula {		

		@Override
		public void executar() {
		Lutador[] lutador = new Lutador[6];
		
		lutador[0] = new Lutador("Pretty Boy", "Fran�a", 31, 1.75f, 68.9f, 11, 1, 2);
		lutador[1] = new Lutador("Putscript", "Brasil", 29, 1.68f, 57.8f, 14, 3, 2);
		lutador[2] = new Lutador("Snapshadow", "EUA", 35, 1.65f, 80.9f, 12, 1, 2);
		lutador[3] = new Lutador("Deep Code", "Austr�lia", 28, 1.93f, 81.6f, 13, 2, 0);
		lutador[4] = new Lutador("UFOCobol", "Brasil", 37, 1.70f, 119.3f, 5, 3, 4);
		lutador[5] = new Lutador("Nerdart", "EUA", 30, 1.81f, 105.7f, 12, 4, 2);
			
		Luta UEC01 = new Luta();
		UEC01.marcarLuta(lutador[4], lutador[5]);
		UEC01.lutar();
		
	}

}
