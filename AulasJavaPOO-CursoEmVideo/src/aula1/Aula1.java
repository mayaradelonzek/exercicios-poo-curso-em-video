package aula1;

import aula_intf.Aula;

public class Aula1 implements Aula {

	@Override
	public void executar() {
		Exercicios.executarExercicio1();
		Exercicios.executarExercicio2();
		Exercicios.executarExercicio3();
	}
	
}
