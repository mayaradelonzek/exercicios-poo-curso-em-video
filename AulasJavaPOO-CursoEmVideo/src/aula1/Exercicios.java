package aula1;

public class Exercicios {

	public static void executarExercicio1() {
		System.out.println("----- Aula1 - Exercicio 1 -----\n");
		System.out.println("Ol�, Eclipse!!!");
	}
	
	public static void executarExercicio2() {
		System.out.println("\n----- Aula1 - Exercicio 2 -----\n");
		Carro meuCarro = new Carro();
		meuCarro.modelo = "Palio";
		meuCarro.anoDeFabricacao = 2011;
		meuCarro.fabricante = "Fiat";
		meuCarro.cor = "Prata";
		
		
		Carro seuCarro = new Carro();
		seuCarro.modelo = "Civic";
		seuCarro.anoDeFabricacao = 2009;
		seuCarro.fabricante = "Honda";
		seuCarro.cor = "Preto";
		
		System.out.println("Meu carro");
		System.out.println("-------------------------------");
		System.out.println("Modelo: " + meuCarro.modelo);
		System.out.println("Ano: " + meuCarro.anoDeFabricacao);
		
		System.out.println();
		
		System.out.println("Seu carro");
		System.out.println("-------------------------------");
		System.out.println("Modelo: " + seuCarro.modelo);
		System.out.println("Ano: " + seuCarro.anoDeFabricacao);
	}
	
	public static void executarExercicio3() {
		System.out.println("\n----- Aula1 - Exercicio 3 -----\n");
		Proprietario dono1 = new Proprietario();
		dono1.nome = "Joao da Silva";
		dono1.cpf = "000.000.000-11";
		dono1.idade = 18;
		dono1.logradouro = "Irmao Jose Otao";
		dono1.bairro = "Independencia";
		dono1.cidade = "Porto Alegre";
		
		Carro meuCarro = new Carro();
		meuCarro.anoDeFabricacao = 2011;
		meuCarro.cor = "Prata";
		meuCarro.modelo = "Palio";
		meuCarro.fabricante = "Fiat";
		meuCarro.dono = dono1;
	}
	
}
