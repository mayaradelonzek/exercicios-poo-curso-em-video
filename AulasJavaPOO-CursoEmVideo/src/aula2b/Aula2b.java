package aula2b;

import aula_intf.Aula;

public class Aula2b implements Aula {
	
	@Override
	public void executar() {
		Cachorro cachorro = new Cachorro();
		Cachorro cachorro2 = new Cachorro();
		
		cachorro.nome = "Bidu";
		cachorro.sexo = "Macho";
		cachorro.idade = 2;
		cachorro.raca = "SRD";
		
		System.out.println("------------------");
		System.out.println("Paciente 1");
		System.out.println("------------------");
		System.out.println("Nome: " + cachorro.nome);
		System.out.println("Sexo: " + cachorro.sexo);
		System.out.println("Idade: " + cachorro.idade);
		System.out.println("Raca: " + cachorro.raca);
		System.out.println("------------------");
		
		System.out.println();
		System.out.println();
		
		cachorro2.nome = "Scooby";
		cachorro2.sexo = "Macho";
		cachorro2.idade = 5;
		cachorro2.raca = "SRD";
		
		System.out.println("------------------");
		System.out.println("Paciente 2");
		System.out.println("------------------");
		System.out.println("Nome: " + cachorro2.nome);
		System.out.println("Sexo: " + cachorro2.sexo);
		System.out.println("Idade: " + cachorro2.idade);
		System.out.println("Raca: " + cachorro2.raca);
		System.out.println("------------------");
	}
		
}