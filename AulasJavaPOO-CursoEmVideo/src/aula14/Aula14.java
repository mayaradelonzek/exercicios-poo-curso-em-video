package aula14;

import aula_intf.Aula;

public class Aula14 implements Aula {		

		@Override
		public void executar() {
		Video v[] = new Video[3];
		v[0] = new Video("Aula 1 de POO");
		v[1] = new Video("Aula 2 de POO");
		v[2] = new Video("Aula 3 de POO");		
		
		Gafanhoto g[] = new Gafanhoto[2];
		g[0] = new Gafanhoto("Maria", 18, "F", "Mariazinha");
		g[1] = new Gafanhoto("Pedro", 26, "M", "Pedrinho Bola");
		
		Visualizacao visualizacao[] = new Visualizacao[5];
		visualizacao[0] = new Visualizacao(g[0], v[2]);
		visualizacao[0].avaliar();		
		System.out.println(visualizacao[0]);
		
		visualizacao[1] = new Visualizacao(g[0], v[1]);
		visualizacao[1].avaliar(87.0f);
		System.out.println(visualizacao[1]);
				
		/*System.out.println("V�DEOS\n--------------------");
		System.out.println(v[0]);
		System.out.println(v[1]);
		System.out.println(v[2]);
		System.out.println("\nGAFANHOTOS\n--------------------");
		System.out.println(g[0]);
		System.out.println(g[1]);*/
		
	}
}
