package aula5b;

public class Conta implements Sistema {
	//Atributos
	private String tipoConta;
	private float saldo;
	private float taxaManutencao;
	private Cliente cliente;
	private boolean aberta;
	
	
	//M�todos especiais
	public Conta(String tipoConta, float saldo, Cliente cliente) {
		super();
		this.tipoConta = tipoConta;
		this.saldo = saldo;
		this.cliente = cliente;
		this.setTaxaManutencao();
	}


	public String getTipoConta() {
		return tipoConta;
	}


	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;			
		
	}


	public float getSaldo() {
		return saldo;
	}


	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}


	public float getTaxaManutencao() {
		return taxaManutencao;
	}

	public void setTaxaManutencao() {
		if (tipoConta.equals("CC")) {
			this.taxaManutencao = 21f;
		} else if (tipoConta.equals("CP")) {
			this.taxaManutencao = 0;
		}			
	}


	public Cliente getCliente() {
		return cliente;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public boolean getAberta() {
		return aberta;
	}


	public void setAberta(boolean aberta) {
		this.aberta = aberta;
	}

	@Override
	public void abrirConta() {
		this.setAberta(true);
		System.out.println("Conta aberta!");

	}


	@Override
	public void fecharConta() {
		if (this.saldo == 0) {
			this.setAberta(false);
			System.out.println("Conta encerrada!");
		} else {
			this.setAberta(true);
			System.out.println("A conta n�o pode ser encerrada devido a pend�ncias ou saldo em conta. Consulte o gerente.");
		}
		
	}

	@Override
	public void depositar(float deposito) {
		this.saldo += deposito;
		System.out.println("Saldo em conta ap�s dep�sito: " + this.saldo);
	}


	@Override
	public void sacar(float saque) {
		if (this.saldo >= saque) {
			this.saldo -= saque;
			System.out.println("Saldo em conta ap�s saque: " + this.saldo);
		} else {
			System.out.println("Valor indispon�vel para saque.");
		}
	
	}
		
}
