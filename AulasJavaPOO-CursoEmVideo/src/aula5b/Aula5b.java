package aula5b;

import aula_intf.Aula;

public class Aula5b implements Aula {		

		@Override
		public void executar() {
		Cliente[] cliente = new Cliente[2];
		
		cliente[0] = new Cliente("Maria", 26, "F", 09583509211d);
		cliente[1] = new Cliente("Jos�", 57, "M", 111111111);
		
		Conta conta1 = new Conta("CC", 50, cliente[0]);
		conta1.abrirConta();
		conta1.depositar(30);
		conta1.sacar(50);	
		
	}

}
