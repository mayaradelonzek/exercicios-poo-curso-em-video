package aula5b;

public interface Sistema {
	public void abrirConta();
	public void fecharConta();
	public void sacar(float saque);
	public void depositar(float deposito);

}
