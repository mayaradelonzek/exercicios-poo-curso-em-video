package aula5;

public class ContaBanco {
	// Atributos
	public int numConta;
	protected String tipo;
	private String dono;
	private float saldo;
	private boolean status;
	
	//M�todos especiais
	public ContaBanco() {
		super();
		this.setSaldo(0);
		this.setStatus(false);
	}
	
	public int getNumConta() {
		return numConta;
	}
	public void setNumConta(int numConta) {
		this.numConta = numConta;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getDono() {
		return dono;
	}
	public void setDono(String dono) {
		this.dono = dono;
	}
	public float getSaldo() {
		return saldo;
	}
	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}
	public boolean getStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	
	//M�todos personalizados
	public void abrirConta(String tipo) {
		this.setTipo(tipo);
		this.setStatus(true);
		if (tipo == "CC") {
			this.setSaldo(50); // ou pode ser: saldo = 50; � a mesma coisa
		} else if (tipo == "CP") {
			this.setSaldo(150);
		}
		
	}
	
	public void fecharConta() {
		if (this.getSaldo() == 0) {
			this.setStatus(false);
			System.out.println("Conta fechada com sucesso!");
		} else {
			System.out.println("Erro! Conta indisponivel para exclusao! Fale com o gerente."); // o ideal e nao escrever dentro de um metodo
		}
	}
	
	public void depositar(float depositarValor) {
		if (this.getStatus() == true) {
			this.setSaldo(this.getSaldo() + depositarValor); // isso � igual a saldo = saldo + depositar
			System.out.println("Deposito realizado na conta de " + this.getDono());
		} else {
			System.out.println("Impossivel depositar! Conta fechada.");
		}
		
	}
	
	public void sacar(float sacar) {
		if (this.getStatus() == true) {
			if (this.getSaldo() >= sacar) {
				this.setSaldo(this.getSaldo() - sacar);
				System.out.println("Saque realizado na conta de " + this.getDono());
			} else {
				System.out.println("Saldo insuficiente!");
			}
		} else {
			System.out.println("Impossivel sacar! Conta fechada.");
		}
	}
	
	public void pagarMensal(float pagar) {
		float valorMensalidade = 0;
		if (this.getTipo() == "CC") {
			valorMensalidade = 12f;
		} else if (this.getTipo() == "CP") {
			valorMensalidade = 20f;
		}
		
		if (this.getStatus() == true) {
			if (this.getSaldo() > valorMensalidade) {
				this.setSaldo(this.getSaldo() - valorMensalidade);
				System.out.println("Mensalidade paga com sucesso por: " + this.getDono());
			} else {
				System.out.println("Saldo insuficiente!");
			}
		} else {
			System.out.println("Impossivel pagar! Conta fechada.");
		}
		
		
	}
		
	public void estadoAtual() {
		System.out.println("SOBRE A CONTA:");
		System.out.println("Conta: " + this.getNumConta());
		System.out.println("Tipo: " + this.getTipo());
		System.out.println("Dono: " + this.getDono());
		System.out.println("Saldo: " + this.getSaldo());
		System.out.println("Status: " + this.getStatus());
		
	}
	
	
	

}
