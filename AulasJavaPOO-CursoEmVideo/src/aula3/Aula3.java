package aula3;

import aula_intf.Aula;

public class Aula3 implements Aula {
	
	@Override
	public void executar() {
		Caneta c1 = new Caneta();		
		c1.modelo = "Bic cristal";
		c1.cor = "Preta";
		//c1.ponta = 0.5;
		c1.carga = 30;
		//c1.tampada = false;
		c1.destampar();
		c1.status();
		c1.rabiscar();				
	
	}
		
}
