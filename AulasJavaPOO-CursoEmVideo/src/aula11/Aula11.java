package aula11;

import aula_intf.Aula;

public class Aula11 implements Aula {		

		@Override
		public void executar() {
		Visitante v1 = new Visitante();
		v1.setNome("Pablo");
		v1.setIdade(14);
		v1.setSexo("M");
		
		System.out.println(v1.toString());
		
		Aluno aluno = new Aluno();
		aluno.setIdade(22);
		aluno.setNome("Ad�o");
		aluno.setSexo("M");
		aluno.setCurso("Engenharia");
		aluno.setMatricula(151151598);		
		System.out.println(aluno);
		aluno.pagarMensalidade();
		
		Bolsista bolsista = new Bolsista();
		bolsista.setNome("Patr�cia");
		bolsista.setMatricula(1411711);
		bolsista.setCurso("Inform�tica");
		bolsista.setIdade(21);
		bolsista.setSexo("F");
		bolsista.setBolsa(12.5f);
		bolsista.pagarMensalidade();
		
		Tecnico tecnico = new Tecnico();
		tecnico.setNome("Carlos");
		tecnico.praticar();
		
		Professor professor = new Professor();
		professor.setNome("Joaquim");
		professor.setSalario(2500);
		professor.receberAumento(450);
		
	}

}
