package com.gerenciador_conta_banco.main;

import com.gerenciador_conta_banco.modelo.Usuario;

public class Inicializador {

	public static void main(String[] args) {
		Usuario usuario1 = new Usuario("Jos�", 09835444602d); 
		usuario1.criarConta("CC");
		usuario1.sacar(40);
		usuario1.depositar(50);
		usuario1.sacar(40);
		usuario1.fecharConta();

	}

}
