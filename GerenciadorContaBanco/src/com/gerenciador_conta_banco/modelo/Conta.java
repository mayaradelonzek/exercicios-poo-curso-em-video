package com.gerenciador_conta_banco.modelo;

public class Conta {
	//Atributos
	private String tipo;
	private float saldo;
	private float taxaManutencao;
	private boolean aberta;
	
	//M�todos especiais
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public float getSaldo() {
		return saldo;
	}
	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}
	public float getTaxaManutencao() {
		return taxaManutencao;
	}
	private void setTaxaManutencao() {
		
		if (this.tipo.equals("CC")) {
			this.taxaManutencao = 21f;
		} else if (this.tipo.equals("CP")){
			this.taxaManutencao = 0;
		}
	}
	public boolean getAberta() {
		return aberta;
	}
	public void setAberta(boolean aberta) {
		this.aberta = aberta;
	}
		
	public Conta(String tipo) {
		super();
		this.tipo = tipo;
		this.aberta = true;
		setTaxaManutencao();
	}
	
	public void depositar(float deposito) {
		if (deposito <= 0) {
			System.out.println("Valor de dep�sito inv�lido!");
		} else {
			this.saldo += deposito;
			System.out.println("Saldo ap�s dep�sito: " + this.saldo);
		}
	}
	
	public void sacar(float saque) {
		if (saque <= saldo) {
			this.saldo -= saque;
			System.out.println("Saldo ap�s saque: " + this.saldo);
		} else {
			System.out.println("Valor de saque inv�lido!");
		}
		
	}
	
	public void fechar() {
		if (this.saldo != 0)  {
			System.out.println("Op��o indispon�vel! Procure o gerente.");
			
		} else {
			this.aberta = false;
			
		}
	}
	
	

}
