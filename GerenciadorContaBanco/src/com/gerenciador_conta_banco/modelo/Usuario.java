package com.gerenciador_conta_banco.modelo;

public class Usuario {
	//Atributos
	private String nome;
	private double cpf;
	private Conta conta; //Agrega��o
	
	//M�todos especiais
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double getCpf() {
		return cpf;
	}
	public void setCpf(double cpf) {
		this.cpf = cpf;
	}
	public Conta getConta() {
		return conta;
	}
	public void setConta(Conta conta) {
		this.conta = conta;
	}
	
	public void criarConta(String tipoConta) {
		this.conta = new Conta(tipoConta);
	}
	
	public Usuario(String nome, double cpf) {
		super();
		this.nome = nome;
		this.cpf = cpf;
	}
	
	public void sacar(float saque) {
		this.conta.sacar(saque);
	}
	
	public void depositar(float deposito) {
		this.conta.depositar(deposito);
	}
	
	public void fecharConta() {
		this.conta.fechar();
	}
	
	

}
