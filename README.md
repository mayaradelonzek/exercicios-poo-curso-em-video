Projeto desenvolvido durante o curso de POO Java do **Curso em Vídeo** (https://www.cursoemvideo.com/course/java-poo/).

**Nos exercícios foram aplicados os seguintes conceitos:**
* Classes e objetos em Java;
* Visibiliade de atributos e métodos;
* Métodos getters, setters e construtor;
* Pilares da POO: encapsulamento, herança e polimorfismo;
* Relacionamento entre classes;
* Agregação entre objetos com Java;
* Projeto final desenvolvido na aula 14;
* Projeto adicional de uma conta bancária no exercício _GerenciadorContaBanco_.

**Ferramentas utilizadas:**

* Eclipse IDE Versão: 2020-12 (4.18.0);
* Java Versão 1.8;
* Windows 10 Versão 1909 (OS Build 18363.1379);

**Para visualizar as funcionalidades dos exercícios realizados durante as aulas, importe cada projeto para uma IDE e execute-os.**

